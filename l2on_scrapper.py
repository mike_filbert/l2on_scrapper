# -*- coding: utf-8 -*-
"""
Created on Sun Sep  3 06:24:25 2017

@author: FilbertMD
"""

import pandas as pd
from selenium import webdriver
from bs4 import BeautifulSoup
import time
from datetime import datetime
from openpyxl import load_workbook

class L2onCrawler():
      def __init__(self, server_name, output_path):
            self.now = datetime.today().strftime("%d.%m.%y")
            self.servers = {'Einhasad': 1091}
            self.items = list(range(1864,1890))
            self.cols_sell = ['date', 'name', 'max_price_buy','min_price_sell',
                              'avg_price_buy','avg_price_sell',
                              'total_count_buy',
                              'total_count_sell', 'avg_marginal',
                              'extreme_marginal']
            self.table = pd.DataFrame(columns = self.cols_sell)
            self.driver = webdriver.Chrome("C:\\Users\\FilbertMD\\Documents\\"
                                           "chromedriver.exe")
            self.server_id = self.servers[server_name]
            self.output = output_path
            self.no_data = []

      def get_frames(self, item_id):
            self.url = ('http://l2on.net/?c=market&a=item&'
                        'id={}&setworld={}'.format(item_id, self.server_id))
            self.driver.get(self.url)
            time.sleep(2)
            self.htmlSource = self.driver.page_source
            self.frames = pd.read_html(self.htmlSource,
                                       attrs={'class': 'tablesorter'})


      def do_calculations(self):
            self.item_name = BeautifulSoup(self.htmlSource, 'lxml').find(name='h1',
                                    attrs={'style':
                                          'margin:0px;padding:0px;'}).text
            print('..now looking for {}'.format(self.item_name))
            sell = self.frames[0].iloc[:, [1,2]]
            buy = self.frames[1].iloc[:, [1,2]]
            sell_prices = sell.iloc[:, 0].astype(str).str.replace(" ","").astype(int)
            buy_prices = buy.iloc[:, 0].astype(str).str.replace(" ","").astype(int)
            sell_count = sell.iloc[:, 1].astype(str).str.replace(" ","").astype(int)
            buy_count = buy.iloc[:, 1].astype(str).str.replace(" ","").astype(int)
            self.max_price_buy = buy_prices.max()
            self.min_price_sell = sell_prices.min()
            self.total_count_buy = buy_count.sum()
            self.total_count_sell = sell_count.sum()
            self.avg_price_buy = ((buy_prices * buy_count).sum()
                                                      / self.total_count_buy)
            self.avg_price_sell = ((sell_prices * sell_count).sum()
                                                      / self.total_count_sell)
            self.avg_marginal = self.avg_price_sell - self.avg_price_buy
            self.extreme_marginal = self.min_price_sell - self.max_price_buy

      def fill_table(self):

            df = pd.DataFrame({self.cols_sell[i] : [n]
                               for i, n in enumerate([self.now, self.item_name,
                               self.max_price_buy,
                               self.min_price_sell, self.avg_price_buy,
                               self.avg_price_sell, self.total_count_buy,
                               self.total_count_sell, self.avg_marginal,
                               self.extreme_marginal])},
                        columns = self.cols_sell)
            self.table = self.table.append(df)

      def excel_update(self):
            book = load_workbook(self.output)
            writer = pd.ExcelWriter(self.output, engine='openpyxl')
            writer.book = book
            writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
            self.table.to_excel(writer, header=False, index=False)
            writer.save()
            print('file updated!')

      def crawl(self):
            for item_id in self.items:
                  try:
                        self.get_frames(item_id)
                        self.do_calculations()
                        self.fill_table()
                  except(ValueError):
                        self.no_data.append(self.item_name)
                        pass
            self.excel_update()
            self.driver.quit()
            print('There are no data for', ', '.join(set(self.no_data)))

if __name__ is '__main__':
      crawler = L2onCrawler('Einhasad', 'C:\\Users\\FilbertMD\\Documents\\'
                                                          'l2on_test.xlsx' )
      crawler.crawl()

